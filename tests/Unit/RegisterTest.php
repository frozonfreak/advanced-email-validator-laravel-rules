<?php

namespace Tests\Unit;

use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * Test email is not longer than 255.
     *
     * @return void
     */
    public function test_email_should_not_be_too_long()
    {
        $response = $this->post('/register', [
            'name' => 'johndoe',
            'email' => str_repeat('a', 247).'@test.com', // 256
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => 'The email may not be greater than 255 characters.'
        ]);
    }

    /**
     * Test email is required.
     *
     * @return void
     */
    public function test_email_is_required()
    {
        $response = $this->post('/register', [
            'name' => 'johndoe',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => 'The email field is required.'
        ]);
    }

    /**
     * Test email format.
     *
     * @return void
     */
    public function test_email_format()
    {
        $response = $this->post('/register', [
            'name' => 'johndoe',
            'email' => 'test.com',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    /**
     * Test email from temporary domain.
     *
     * @return void
     */
    public function test_email_from_temporary_domain()
    {
        $response = $this->post('/register', [
            'name' => 'johndoe',
            'email' => 'test@0clickemail.com',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => 'Emails must not be from temporary email service provider'
        ]);
    }


    /**
     * Test email not from top level domain
     *
     * @return void
     */
    public function test_email_not_from_top_level_domain()
    {
    	$email = 'test@gmail.aaaa';
    	$domain = explode('@', $email)[1];
    	$domain_extension_arr = explode('.', $domain);
    	$domain_extension = $domain_extension_arr[count($domain_extension_arr) - 1];

        $response = $this->post('/register', [
            'name' => 'johndoe',
            'email' => $email,
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => '"'.$domain_extension.'" not a top level domain'
        ]);
    }


    /**
     * Test email domain is pingable
     *
     * @return void
     */
    public function test_email_not_pingable()
    {
    	$email = 'test@gmail1.com';
    	$domain = explode('@', $email)[1];

        $response = $this->post('/register', [
            'name' => 'johndoe',
            'email' => $email,
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'email' => 'Unable to reach domain "'.$domain.'"'
        ]);
    }
}
