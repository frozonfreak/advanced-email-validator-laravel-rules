<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Ping;

class PingableEmailDomainValidator implements Rule
{
    /**
     * The Domain.
     *
     * @var string
     */
    public $domain;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
            return false;
        }
        
        $this->domain = explode('@', $value)[1];
        $health = Ping::check($this->domain);

        if($health !== 200) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Unable to reach domain "'.$this->domain.'"';
    }
}
