# Advanced Email Validator - Laravel

## Introduction

Supercharge Laravel e-mail validation. Increase user conversion by allowing only valid emails into system.

## Features
- Modular Laravel Rules
- Easy deployment to existing projects
- Reject emails from 3000+ Temporary Email service provider
- Allow emails only from 1500+ top level domains 
- Check if an email domain is valid (Ping the domain and check if it returns 200)


## Installation

### New Project
- This package is built as barebone code, can be used as is for new project 
- Unzip the package 
- Change directory to package name `cd <folder_name>`
- Run `composer update`
- Start your new project
- Import following rules into your Request / Validator 
--`use App\Rules\TempEmailServiceValidator;`
--`use App\Rules\TopLevelEmailDomainValidator;`
--`use App\Rules\PingableEmailDomainValidator;`
- Add rules to validation
-- `new TempEmailServiceValidator(),` - Validate for temporary email service provider
-- `new TopLevelEmailDomainValidator(),` - Validate for top level domains
-- `new PingableEmailDomainValidator(),` - Check if email domain is valid

### Existing Project
#### Step 1 - Add dependencies
Install via Composer:
```bash
composer require karlmonson/laravel-ping
```
#### Step 2 - Register component
You'll need to register the ServiceProvider and Facade:
```php
// config/app.php

'providers' => [
    // ...
    Karlmonson\Ping\PingServiceProvider::class,
];

'aliases' => [
    // ...
    'Ping' => Karlmonson\Ping\Facades\Ping::class,
];
```
#### Step 3 - Copy Rules
- Copy all Rules from `app\Rules` and paste into Rules folder of your project
- Import following rules into your Request / Validator 
-- `use App\Rules\TempEmailServiceValidator;`
-- `use App\Rules\TopLevelEmailDomainValidator;`
-- `use App\Rules\PingableEmailDomainValidator;`
- Add rules to validation
-- `new TempEmailServiceValidator(),` - Validate for temporary email service provider
-- `new TopLevelEmailDomainValidator(),` - Validate for top level domains
-- `new PingableEmailDomainValidator(),` - Check if email domain is valid

#### Step 4 - Test Case
- Copy test cases from `Unit/RegisterTest.php` to your test cases

## Usage

```php
<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

// Custom Rules
use App\Rules\TempEmailServiceValidator;
use App\Rules\TopLevelEmailDomainValidator;
use App\Rules\PingableEmailDomainValidator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => ['required',
                        'string',
                        'email',
                        'max:255',
                        // Filter temp email domains
                        new TempEmailServiceValidator(),
                        // Check for top level domains
                        new TopLevelEmailDomainValidator(),
                        // Check if domain is active
                        new PingableEmailDomainValidator(),
                        'unique:users'
                      ],
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}

```